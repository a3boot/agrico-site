// scroll to top
$(document).ready(function(){
    $(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });
    $('.scrollup').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });
});


//    scroll
$(document).ready(function(){
    $("#menu").on("click","a", function (event) {

        event.preventDefault();

        var id  = $(this).attr('href'),

            top = $(id).offset().top;

        $('body,html').animate({scrollTop: top}, 1500);
    });
});

// header size
$(document).ready(function wResize () {
    if(screen.height>0) {
        $('#header').css('min-height', $(window).height());
    }
    $(window).resize(function () {
        wResize()
    })
});


// spoiler

function showText(num) {
    for (i=0; i<=2; i++) {
        if (i!=num){
            document.getElementsByClassName('news__block-hiden-text')[i].style.display='none';
            document.getElementsByClassName('news__block-content-hidden')[i].style.display='block';
            document.getElementsByClassName('news__block-switch')[i].style.display='block';
        }
        else if (i == num) {
            document.getElementsByClassName('news__block-hiden-text')[i].style.display='block';
            document.getElementsByClassName('news__block-content-hidden')[i].style.display='none';
            document.getElementsByClassName('news__block-switch')[i].style.display='none';
        }
    }
}
function hideText(num) {
    for (i=0; i<=2; i++) {
        if (i == num){
            document.getElementsByClassName('news__block-hiden-text')[i].style.display='none';
            document.getElementsByClassName('news__block-content-hidden')[i].style.display='block';
            document.getElementsByClassName('news__block-switch')[i].style.display='block';
        }
    }
}
