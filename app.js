var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var fs = require('fs');
var xml2js = require('xml2js');
var pool = require('./bin/conn/index');
var secondPool = require('./bin/conn/index');
var util = require('util');
var Busboy = require('busboy');
var Excel = require('exceljs');
var routes = require('./routes/index');
var users = require('./routes/users');
var shop = require('./routes/shop');
var admin = require('./routes/admin');
var config=require('./config');
var session=require('express-session');
var lang = require('./bin/langKey');
var app = express();

var parser = new xml2js.Parser();
//load files

app.post('/loadFile', function(req,res,next) {

    var fileNameR='';

    var busboy = new Busboy({ headers: req.headers });
    busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
        fileNameR = filename;
        if (fileNameR.split('.')[1]=='xml' ||fileNameR.split('.')[1]=='XML') {
            file.on('data', function(data) {
                    //Function read XML-file
                    selectingManufacturerXml(data);
            });
        }else{
            Path=__dirname+'/file';
            console.log('Path ' + Path);
            console.log('path.basename(fieldname) ' + path.basename(fieldname));

            var saveTo = path.join(Path, path.basename(fieldname));
            console.log('saveTo ' + saveTo);
            var stream=fs.createWriteStream(saveTo);
            file.pipe(stream);
            stream.on('finish', function() {
                console.log('stream [' + fieldname + '] Finished');
                selectingManufacturerXml();
            });
        }

    });


    req.pipe(busboy);


    function selectingManufacturerXml(data) {

        var articulsdbArr = [];
        var namedbArr = [];
        pool.pool.query('select * from goods', function (err,rows,field) {
            if (err){
                callback(err,null);
            }else {
                for (var i = 0; i < rows.length; i++) {

                    articulsdbArr.push(rows[i].articul);
                    namedbArr.push(rows[i].name);
                }
                if(data) {
                    importFromFileXml(err, articulsdbArr, namedbArr, data);
                }else {
                    importFromFileXls(err, articulsdbArr, namedbArr);
                }
            }

        });

    }

    var  textContent='';


    //Function callback
    function importFromFileXml (err,articulsdbArr,namedbArr,data) {
        if (err) {
            console.log("ERROR : ", err);
        } else {

            // fs.open('table.txt', "a+", 0644, function(err, file_handle) {
            //     if (!err) {
            //         console.log('table.txt');
            var manufacturerdbArr = [];
            //Function select manufacturers from manufacturer
            //and push them into db array

            pool.pool.query('select * from manufacturer', function (err, rows, field) {
                if (err) throw err;
                for (var i = 0; i < rows.length; i++) {
                    manufacturerdbArr[rows[i].id] = rows[i].name.toUpperCase();

                }

                parser.parseString(data, function (err, result) {
                    var root = result.root;
                    var manufacturerXmlArr = [];
                    var articulsXmlArr = [];
                    var nameXmlArr = [];

                    // loop push elements from XML file into arrays
                    for (var u = 0; u < root.element.length; u++) {
                        manufacturerXmlArr.push(root.element[u].$.manufacturer);
                        articulsXmlArr.push(root.element[u].$.articul);
                        nameXmlArr.push(root.element[u].$.name);
                    }
                    //function search difference between XMl arrays and db arrays
                    Array.prototype.diff = function (a) {
                        return this.filter(function (u) {
                            return a.indexOf(u) < 0;
                        });
                    };
                    var resultManufacturer = manufacturerXmlArr.diff(manufacturerdbArr);
                    for (var i = 0; i < resultManufacturer.length; i++)
                        for (var j = i + 1; j < resultManufacturer.length;)
                            if (resultManufacturer[i] == resultManufacturer[j]) resultManufacturer.splice(j, 1);
                            else j++;
                    //loop insert data into goods
                    for (var y = 0; y < articulsXmlArr.length; y++) {

                        var isExists = false;
                        for (var k = 0; k < articulsdbArr.length; k++) {
                            if (articulsXmlArr[y] == articulsdbArr[k]) {
                                isExists = true;
                                pool.pool.query('update goods set name ="' + nameXmlArr[y] + '" where articul="' + articulsXmlArr[y] + '"', {});
                                pool.pool.query('update goods set id_manufacturer="' + manufacturerdbArr.indexOf(manufacturerXmlArr[y].toUpperCase().toString()) + '" where articul="' + articulsXmlArr[y] + '"', {});
                            }
                        }
                        if (!isExists) {
                            pool.pool.query('insert into goods set ?', {
                                id_manufacturer: manufacturerdbArr.indexOf(manufacturerXmlArr[y].toUpperCase().toString()),
                                articul: articulsXmlArr[y],
                                name: nameXmlArr[y]
                            });
                        }
                        //loop insert data into manufacturer
                    }
                    for (var z = 0; z < resultManufacturer.length; z++) {
                        pool.pool.query('insert into manufacturer set ?', {
                            name: resultManufacturer[z]
                        });
                    }
                });
            });
        }
    }

    function importFromFileXls(err,articulsdbArr,namedbArr) {
        if (err) {
            console.log(err);
        }else {
            var manufacturerdbArr = [];
            pool.pool.query('select * from manufacturer', function (err, rows, field) {
                if (err) throw err;
                for (var i = 0; i < rows.length; i++) {
                    manufacturerdbArr[rows[i].id] = rows[i].name.toUpperCase();

                }
                fs.readFile('file/file', function (err, data) {
                    if (err)
                        console.log(err);
                    //work with excel
                    var workbook = new Excel.Workbook();

                    workbook.xlsx.readFile('file/file')
                        .then(function () {
                            var worksheet = workbook.getWorksheet(1);
                            worksheet.eachRow(function (row, rowNumber) {
                                if (manufacturerdbArr.indexOf(row.values[4].toUpperCase().toString()) != (-1)) {
                                    if (articulsdbArr.indexOf(row.values[1].toString()) != (-1)) {
                                        pool.pool.query('update goods set name ="' + row.values[2] + '" where articul="' + row.values[1] + '"', {});
                                    }
                                    else {
                                        pool.pool.query('insert into goods set ?', {
                                            id_manufacturer: manufacturerdbArr.indexOf(row.values[4].toUpperCase().toString()),
                                            articul: row.values[1],
                                            name: row.values[2]
                                        });
                                    }

                                }
                                else {
                                    textContent += 'Row ' + rowNumber + ' = ' + JSON.stringify(row.values) + "\n";
                                }

                            });
                            fs.writeFile('table.txt', textContent, 'utf-8', function (err, written) {
                                if (!err) {
                                } else {

                                }
                            });

                        });

                });
            });
        }
        res.redirect('back');
    }
});

// view engine setup
app.set('views', path.join(__dirname, 'views/pages'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({secret: 'ssshhhhh1',saveUninitialized: true,resave: true}));
app.use(bodyParser.urlencoded({extended: true}));

app.use('/', routes);
app.use('/users', users);
app.use('/shop', shop);
app.use('/admin', admin);

app.get('/ua', function (req,res,next) {
    req.session.lang = 'ua';
    res.redirect('back');
});

app.get('/ru', function (req,res,next) {
    req.session.lang = 'ru';
    res.redirect('back');
});

//search goods
app.post('/search',function (req,res,next) {


    if (req.body.counter=='undefined'){
        req.session.counter=0;
        counter=0;
    }
    else {
        counter = +req.body.counter;

        var check=req.session.counter=+req.session.counter+counter;
        if (check < 0){

            req.session.counter=0;
        }
    }

    var goods ='', select='select * from goods'
    var goods='', select='SELECT  goods.articul, goods.name, manufacturer.name AS m_name from goods ' +
        'LEFT JOIN manufacturer  ON goods.id_manufacturer=manufacturer.id WHERE  (';

    select+=' manufacturer.name="'+req.body.manufacturer+'" )';

    if (req.body.searchValue!=''){

        select+='  AND goods.articul LIKE "%'+req.body.searchValue+'%" limit '+req.session.counter+', 50';

    } else {
        select +=' limit '+req.session.counter+', 50';
    }

    secondPool.secondPool.query(select, function (err, rows, field) {
        if (err) throw err;
        var j=0;
        for (var i = 0; i < rows.length; i++) {
            j=i+1+req.session.counter;
            goods +=  '<tr><td>' + j + '</td>' + '<td>' + rows[i].articul + '</td>' +
                '<td>' + rows[i].name + '</td>' +
                '<td>' + rows[i].m_name.toUpperCase() + '</td></tr>';
            // goods += '<li>'  + '<span class=" col-md-1 number" >' + j + '</span>' +
            //     '<span class="col-md-3 articul">' + rows[i].articul + '</span>' +
            //     ' <span class="col-md-5 name">' + rows[i].name + '</span> ' +
            //     '<span class="col-md-3 manufact">' + rows[i].m_name.toUpperCase() + '</span>' + '</li>';
        }

        if (req.session.counter==0 && j%50==0 && j!=0){
            goods+='<tr><td></td><td></td><td>'+
                ' <div class="row">'+
                '<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">'+
                '<input type="button" value="Назад" class="next" onclick="searchGoods(window.location.hash.slice(1).toUpperCase(),\'-50\')" disabled>'+
                '</div>'+
                '<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">'+
                '<input type="button" value="Вперед" class="next" onclick="searchGoods(window.location.hash.slice(1).toUpperCase(),\'50\')" >'+
                '</div>'+
                '</div>'+
                '</td><td></td></tr>';

        }
        else if(req.session.counter!=0 && j%50!=0 && j!=0)
        {
            goods+='<tr><td></td><td></td><td>'+
                ' <div class="row">'+
                '<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">'+
                '<input type="button" value="Назад" class="next" onclick="searchGoods(window.location.hash.slice(1).toUpperCase(),\'-50\')">'+
                '</div>'+
                '<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">'+
                '<input type="button" value="Вперед" class="next" onclick="searchGoods(window.location.hash.slice(1).toUpperCase(),\'50\')" disabled>'+
                '</div>'+
                '</div>'+
                '</td><td></td></tr>';
        }
        else if(req.session.counter!=0 && j%50==0 && j!=0)
        {
            goods+='<tr><td></td><td></td><td>'+
                ' <div class="row">'+
                '<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">'+
                '<input type="button" value="Назад" class="next" onclick="searchGoods(window.location.hash.slice(1).toUpperCase(),\'-50\')">'+
                '</div>'+
                '<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">'+
                '<input type="button" value="Вперед" class="next" onclick="searchGoods(window.location.hash.slice(1).toUpperCase(),\'50\')">'+

                '</div>'+
                '</div>'+
                '</td><td></td></tr>';
        }
        res.send(goods);
    });
});
//verification admin
app.post('/verification', function(req,res,next) {


    if (req.body.login=='A' && req.body.password=='1'){
        req.session.name=req.body.login;
        res.send('ok');
    }
    else{
        res.send('Невірний логін або пароль');
    }
});
// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;