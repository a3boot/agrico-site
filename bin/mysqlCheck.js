var sql = require("mysql");
var config = require('../config');
//-------------------------------------------
//Create connection with db without database define for testing (must be createConnection rather createPool)
//-------------------------------------------
var pool = sql.createConnection({
    connectionLimit : config.connectionLimit,
    host : config.host,
    user : config.user,
    password : config.password
});
function showDb() {
    pool.query("show databases like 'agricogroup' ",function (err, rows) {
        if (err) throw err;
        if (rows.length == 0) {
            console.log(' database "agricogroup" NOT exist ');
            creatDb();
        } else {
            console.log(' database "agricogroup" exist /n');
            process.exit();
        }
    });
}

function creatDb() {
    pool.query('CREATE DATABASE agricogroup CHARACTER SET utf8 COLLATE utf8_general_ci',function (err) {
        if (err) throw err;
        usingDb();
    });

}

function usingDb() {
    pool.query('USE agricogroup ',function (err) {
        if (err) throw err;
        creatTableManufacturer();
        creatTableGoods();
    });
}

function creatTableManufacturer() {
    pool.query(
        "CREATE table manufacturer(id INT(10) AUTO_INCREMENT,name VARCHAR(125) NOT NULL,PRIMARY KEY (id))",
        function (err) {
            if (err) throw err;
            insertIntoManucturer();
        });
}

function creatTableGoods() {
    pool.query(
        "CREATE table goods(id int(10) auto_increment,"+
        "articul varchar(55) not null,"+
        "name varchar(155) not null,"+
        "id_manufacturer int(10) not null,"+
        "primary key(id, articul))",
        function (err) {
            if (err) throw err;
        });
}


function insertIntoManucturer() {
    pool.query(
        "INSERT INTO manufacturer VALUES (1,'CLAAS'),(2,'AKPIL'),(3,'SPICER'),(4,'UNIROL'),(5,'CARRARO'),(6,'AMAZONE'),(7,'HORSCH'),(8,'VADERSTAD'),(9,'KUHN')",
        function (err) {
            if (err) throw err;
            process.exit();
        });
}

showDb();

//-------------------------------------------
//End connection with db
//-------------------------------------------