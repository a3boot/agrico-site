var express = require('express');
var pool=require('../bin/conn/index');
var router = express.Router();

var lang = require('../bin/langKey');
/* GET home page. */
router.get('/', function(req, res, next) {

    req.session.lang = req.session.lang || 'ua';
    languageSess = lang.funcLang(req.session.lang);
    res.render('shop', languageSess);

});
module.exports = router;
