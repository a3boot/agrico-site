var lang = require('../bin/langKey');
module.exports.verification=function (req,res,next,language) {

    if (req.session.name != undefined) {
        verification = '<div class="container">' +
            '<div class="row">' +
            '<div class="col-md-3">' + '</div>' +
            '<div class="col-md-6">' +
            '<div class="select-file">' + language.selectFileInAdminPage + '</div>' +


            '</div>' +
            '<form method="post" action="/loadFile" enctype="multipart/form-data" class="select-file">' +
            '<label>' +
            '<input type="file" name="file" multiple="multiple" class="submit-button">' +
            '</label>' + '<BR>' +
            '<label>' +
            '<input type="submit" value='+language.sendButtonInAdminPage+' class="submit-button">' +
            '</label>' +
            '</form>' +


            '<div class="col-md-3">' + '</div>' +
            '</div>' +
            '</div>';
    }
    else {
        verification = '<div class="container">' +
            '<div class="row">' +
            '<div class="col-md-4">' + '</div>' +
            '<div class="col-md-4">' +
            '<form class="form-signin" role="form" method="post" action="/verification">' +
            '<h2 class="form-signin-heading">' + language.loginName + '</h2>' +
            '<input type="text" class="form-control" placeholder='+language.login+' required autofocus id="login">' +
            '<input type="password" class="form-control" placeholder="Пароль" required id="password">' +
            '<input class="btn btn-lg btn-primary btn-block" type="button" value='+language.authorization+' onclick="verificationPass()">' +
            '</form>' +
            '</div>' +
            '<div class="col-md-4">' + '</div>' +
            '</div>' +
            '</div>';
    }
    return verification;
};