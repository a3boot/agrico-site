var express = require('express');
var router = express.Router();
var session = require('express-session');
var verification=require('./verificationForm');
var lang = require('../bin/langKey');

/* GET users listing. */
router.get('/', function(req, res, next) {
    languageSess = lang.funcLang(req.session.lang);
    var verif=verification.verification(req,res,next,languageSess);
    req.session.lang = req.session.lang || 'ua';

    languageSess.varification = verif;
    res.render('admin',
        languageSess
    );
});

module.exports = router;