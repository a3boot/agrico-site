var express = require('express');
var router = express.Router();

var lang = require('../bin/langKey');
/* GET home page. */
router.get('/', function(req, res, next) {
  req.session.lang = req.session.lang || 'ua';
  languageSess = lang.funcLang(req.session.lang);
  res.render('index', languageSess);
});

module.exports = router;
